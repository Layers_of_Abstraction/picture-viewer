﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Web.UI;
using System.Collections.Generic;
using FullScreen;
using Data;
using System.IO;

namespace PictureViewer
{
    public partial class frmMain : Form
    {
        #region Object Declarations
        ImageData _objImgData = new ImageData();
        frmFullScreen _objFrmFullScreen = new frmFullScreen();
        List<string> _arryFilePaths = null;

        #endregion Object Declarations

        #region Initializers
        public frmMain()
        {
            InitializeComponent();
        }

        //Displays larger instance of selected image in picture box.
        private void lstImages_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < lstImages.SelectedItems.Count; i++)
            {
                //GET filename from listview and store in index.
                _objImgData.strFileName = lstImages.SelectedItems[i].Text;
                //Create larger instance of image.
                pictureBox1.Image = Image.FromFile(_objImgData.strFileName);
                //Fill panel to the width and height of picture box.
                panel1.AutoScrollMinSize = new Size(pictureBox1.Image.Width, pictureBox1.Image.Height);
                _objImgData.intImageIndex = lstImages.SelectedIndices[i];
                loadImageMetaData();
            }
        }        
        #endregion Initializers

        #region Button Events
        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (pictureBox1.Image != null)
                {
                    if (_objImgData.intImageIndex < StaticImageList.Instance.GlobalImageList.Images.Count)
                    {
                        lstImages.Items[_objImgData.intImageIndex].Selected = false;
                        _objImgData.intImageIndex++;
                        lstImages.Items[_objImgData.intImageIndex].Selected = true;
                        lstImages.Select();
                    }
                }
                else
                {
                    MessageBox.Show(_objImgData.strMessage);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR: " + ex.Message);
            }

            finally
            {                
                if (_objImgData.intImageIndex >= StaticImageList.Instance.GlobalImageList.Images.Count)
                {
                    _objImgData.intImageIndex--;
                }
            }            
        }


        private void btnFullScreen_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                _objFrmFullScreen = new frmFullScreen(_objImgData, StaticImageList.Instance.GlobalImageList, lstImages);
                _objFrmFullScreen.Show();
            }

            else
            {
                MessageBox.Show(_objImgData.strMessage);
            }
        }

        private void btnRotateClockwise_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                Bitmap bmp = new Bitmap(pictureBox1.Image);
                bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                pictureBox1.Image = bmp;
            }
            else
            {
                MessageBox.Show("No picture");
            }
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            try
            {
                _objImgData.intImageIndex = StaticImageList.Instance.GlobalImageList.Images.Count - 1;
                lstImages.Items[_objImgData.intImageIndex].Selected = true;
                lstImages.Select();
            }

            catch (Exception ex)
            {
                Debug.WriteLine("ERROR: " + ex.Message);
            }
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                _objImgData.intImageIndex = 0;
                lstImages.Items[_objImgData.intImageIndex].Selected = true;
                lstImages.Select();
            }

            catch (Exception ex)
            {
                Debug.WriteLine("ERROR: " + ex.Message);
            }
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                if (pictureBox1.Image != null)
                {                    
                    if (_objImgData.intImageIndex >= 0 || _objImgData.intImageIndex <= StaticImageList.Instance.GlobalImageList.Images.Count)
                    {
                        //De-select current item
                        lstImages.Items[_objImgData.intImageIndex].Selected = false;
                        //GO to previous entry.
                        _objImgData.intImageIndex--;
                        //Select new item
                        if (_objImgData.intImageIndex >= 0)
                        {
                            lstImages.Items[_objImgData.intImageIndex].Selected = true;
                            lstImages.Select();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No image.");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR: " + ex.Message);
            }

            finally
            {
                if (_objImgData.intImageIndex <= -1)
                {
                    _objImgData.intImageIndex++;
                }
            }
        }

        private void btnRotateAntiClockwise_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                Bitmap bmp = new Bitmap(pictureBox1.Image);                
                bmp.RotateFlip(RotateFlipType.Rotate90FlipXY);                
                pictureBox1.Image = bmp;
            }
            else
            {
                MessageBox.Show("No picture");
            }
        }
        #endregion Button Events

        #region Button Tool Tips
        private void btnFirst_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnFirst, "Go to first record.");
        }

        private void btnPrevious_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnPrevious, "Go to previous record.");
        }

        private void btnNext_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnNext, "Go to next record.");
        }

        private void btnLast_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnLast, "Go to last record.");
        }

        private void btnRotateAntiClockwise_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnRotateAntiClockwise, "Rotate large picture anti clockwise.");
        }

        private void btnRotateClockwise_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnRotateClockwise, "Rotate large picture clockwise.");
        }

        private void btnFullScreen_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnFullScreen, "Enter full screen mode.\nPress Escape to exit.");
        }
        #endregion Button Tool Tips

        #region Mouse Events
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                //If right mouse button is clicked.
                if (e.Button == MouseButtons.Right)
                {
                    //Create bitmap instance from current image.
                    Bitmap bmp = new Bitmap(pictureBox1.Image);
                    //Create second bitmap instance and shrink it. Image will become blank.
                    Bitmap bmp_new = new Bitmap(Convert.ToInt32(pictureBox1.Image.Width / 2),
                                                Convert.ToInt32(pictureBox1.Image.Height / 2));
                    //Store bitmap instance in graphics filter.
                    Graphics gr = Graphics.FromImage(bmp_new);
                    //Redraw bitmap.
                    gr.DrawImage(bmp, 0, 0, bmp_new.Width, bmp_new.Height);
                    //Display bitmap again in picture box.
                    pictureBox1.Image = bmp_new;
                    //Allow scrolling for image.
                    panel1.AutoScrollMinSize = new Size(pictureBox1.Image.Width,
                                                        pictureBox1.Image.Height);
                }

                else if (e.Button == MouseButtons.Left)
                {
                    //Create bitmap instance from current image.
                    Bitmap bmp = new Bitmap(pictureBox1.Image);
                    //Create second bitmap instance and shrink it. Image will become blank.
                    Bitmap bmp_new = new Bitmap(Convert.ToInt32(pictureBox1.Image.Width * 2),
                                                Convert.ToInt32(pictureBox1.Image.Height * 2));
                    //Store bitmap instance in graphics filter.
                    Graphics gr = Graphics.FromImage(bmp_new);
                    //Redraw bitmap.
                    gr.DrawImage(bmp, 0, 0, bmp_new.Width, bmp_new.Height);
                    //Display bitmap again in picture box.
                    pictureBox1.Image = bmp_new;
                    //Allow scrolling for image.
                    panel1.AutoScrollMinSize = new Size(pictureBox1.Image.Width,
                                                        pictureBox1.Image.Height);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR: " + ex.Message);
            }
        }
        #endregion Mouse Events

        #region File Menu Events
        private void mnuSave_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                string save_path = "";
                SaveFD.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                SaveFD.FileName = "default";
                SaveFD.Filter = "JPEG|*.jpeg";

                if (SaveFD.ShowDialog() != DialogResult.Cancel)
                {
                    save_path = SaveFD.FileName;
                    Bitmap bmp = new Bitmap(pictureBox1.Image);
                    bmp.Save(save_path, ImageFormat.Jpeg);
                }
            }

            else
            {
                MessageBox.Show("No Image");
            }
        }
        private void mnuQuit_Click(object sender, EventArgs e)
        {

            DialogResult dialogResult = MessageBox.Show("Quit?", "Are you sure you wish to exit?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                //Close dialog box and continue.
            }
        }

        private void mnuOpen_Click(object sender, EventArgs e)
        {
            loadImageList();
            _objImgData.intImageIndex = 0;
        }
        #endregion File Menu Events

        #region Edit Menu Events
        private void mnuInvert_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                Bitmap bmp = new Bitmap(pictureBox1.Image);

                int x, y;

                for (x = 0; x < bmp.Width; x++)
                {
                    for (y = 0; y < bmp.Height; y++)
                    {
                        //GET pixel width and height.
                        //GET old colour pixel cordinate.
                        Color old_pixel_colour = bmp.GetPixel(x, y);
                        //NEGATE the RBG value of 255 from the current pixel. 
                        Color new_pixel_colour = Color.FromArgb(255 - old_pixel_colour.R,
                                                                255 - old_pixel_colour.G,
                                                                255 - old_pixel_colour.B);
                        //SET pixel with new colour.
                        bmp.SetPixel(x, y, new_pixel_colour);
                    }
                }
                //REDRAW image to picturebox.
                pictureBox1.Image = bmp;
            }

            else
            {
                MessageBox.Show("No Image");
            }
        }

        private void mnuRed(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
            {
                //INSTANTIATE instance for current image in picturebox.
                Bitmap bmp = new Bitmap(pictureBox1.Image);
                //DECLARE loop index for X and Y corrdinates.
                int x, y;
                //COUNT begining to end of pixel width.
                for (x = 0; x < bmp.Width; x++)
                {
                    //COUNT begining to end of pixel Hieght.
                    for (y = 0; y < bmp.Height; y++)
                    {
                        //GET pixel width and height.
                        //GET old colour pixel cordinate.
                        Color old_Pixel_colour = bmp.GetPixel(x, y);
                        //SET new colour red for pixel based on scanned area.
                        Color new_pixel_colour = Color.FromArgb(old_Pixel_colour.R, 0/*B*/, 0/*G*/);
                        //SET pixel with new colour.
                        bmp.SetPixel(x, y, new_pixel_colour);
                    }
                }
                //REDRAW image to picturebox.
                pictureBox1.Image = bmp;
            }

            else
            {
                MessageBox.Show("No image");
            }
        }
        #endregion Edit Menu Events

        #region Help Menu Events
        private void mnuHelp_Click(object sender, EventArgs e)
        {
            //Convert The resource Data into Byte[]
            byte[] PDF = Data.Properties.Resources.UserManual;
            MemoryStream ms = new MemoryStream(PDF);
            //Create PDF File From Binary of resources folders help.pdf
            FileStream f = new FileStream("help.pdf", FileMode.OpenOrCreate);
            //Write Bytes into Our Created help.pdf
            ms.WriteTo(f);
            f.Close();
            ms.Close();
            // Finally Show the Created PDF from resources
            Process.Start("help.pdf");
        }

        private void mnuAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Copyright (C) 2018 Jordan Nash. \nPicture Viewer is licenced under the GNU General Public Licence making this software free.");
        }
        #endregion Help Menu Events

        #region Methods
        public void loadImageList()
        {
            _objImgData.intCounter = 0;

            oFD1.InitialDirectory = "C:\\";
            oFD1.Title = "Open an Image File";
            oFD1.Filter = "JPEGS|*.jpg|GIFS|*.gif|PNGS|*.png|BMPS|*.bmp";

            //Open Dialog Box.
            var oldResults = oFD1.ShowDialog();

            if (oldResults == DialogResult.Cancel)
            {
                return;
            }

            int num_of_files = oFD1.FileNames.Length;
            _arryFilePaths = new List<string>(num_of_files);

            foreach (string single_file in oFD1.FileNames)
            {
                _arryFilePaths.Add(single_file);
                StaticImageList.Instance.GlobalImageList.Images.Add(single_file, Image.FromFile(single_file));
                _objImgData.intCounter++;
            }
            lstImages.LargeImageList = StaticImageList.Instance.GlobalImageList;


            for (int i = 0; i < _objImgData.intCounter; i++)
            {
                //DISPLAY filename and image from image index param. 
                lstImages.Items.Add(_arryFilePaths[i], i);
            }
           
        }

        public void loadImageMetaData()
        {
            lstImageMetaData.Items.Clear();
            Bitmap bmp = new Bitmap(_objImgData.strFileName);
            EXIF.EXIFextractor er2 = new EXIF.EXIFextractor(ref bmp, "\n");
            foreach (Pair s in er2)
            {
                lstImageMetaData.Items.Add(s.First + " : " + s.Second);
            }
            er2.setTag(0x5013, "Thumbnail Width");
            lstImageMetaData.SelectedIndex = lstImageMetaData.Items.Count - 1;
        }

        
        #endregion Methods


    }
}
