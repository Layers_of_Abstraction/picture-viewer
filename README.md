This program was developed in C# using .NET 4.6.1 framework in Visual Studio 2017 and based on Home And Learn’s picture viewer application. If this application will not load then you may need to install .NET 4.6.1. https://www.microsoft.com/en-au/download/details.aspx?id=49981

This program was created for the use as a replacement should the default Windows Picture Viewer not function. It will be used for the primary purpose of allowing user to select and open a series of photos at runtime in a listview box and colour and see them and display them at fullscreen.

The application should be compatible with both the 32bit architecture and the 64bit architecture. 
