﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel;

namespace Data
{
    [ToolboxItem(false)]
    public class StaticImageList : Component
    {
        private ImageList globalImageList;
        public ImageList GlobalImageList
        {
            get
            {
                return globalImageList;
            }
            set
            {
                globalImageList = value;
            }
        }

        private IContainer components;

        private static StaticImageList _instance;
        public static StaticImageList Instance
        {
            get
            {
                if (_instance == null) _instance = new StaticImageList();
                return _instance;
            }
        }

        private StaticImageList()
        {
            InitializeComponent();
        }

        public StaticImageList(ImageList imageList)
        {
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.globalImageList = new System.Windows.Forms.ImageList(this.components);
            // 
            // GlobalImageList
            // 
            this.globalImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.globalImageList.ImageSize = new System.Drawing.Size(24, 24);
            this.globalImageList.TransparentColor = System.Drawing.Color.Transparent;
        }
    }
}
