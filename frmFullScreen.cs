﻿using Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace FullScreen
{
    public partial class frmFullScreen : Form
    {
        #region Object Declarations
        public ImageData _objImageData = null;
        static ListView _listView = null;
        bool _bolFlag = true;
        #endregion Object Declarations

        #region Initializers
        public frmFullScreen()
        {
            InitializeComponent();
        }
        public frmFullScreen(ImageData imageData, ImageList imageList, ListView listView)
        {
            InitializeComponent();
            _listView = listView;
            _objImageData = imageData;
            _objImageData.intImageIndex = 0;
            StaticImageList.Instance.GlobalImageList = imageList;
        }

        private void frmFullScreen_Load(object sender, EventArgs e)
        {
            GoFullscreen(_bolFlag);
            pictureBox1.ImageLocation = _listView.Items[_objImageData.intImageIndex].Text;
        }
        #endregion Initializers

        #region Events
        private void frmFullScreen_keyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Escape)
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                }
                else if (e.KeyData == Keys.Right)
                {
                    if (pictureBox1.Image != null)
                    {
                        if (_objImageData.intImageIndex < StaticImageList.Instance.GlobalImageList.Images.Count)
                        {
                            pictureBox1.Image = Image.FromFile(_listView.Items[_objImageData.intImageIndex].Text);
                            _objImageData.intImageIndex++;
                        }
                        else
                        {
                            _objImageData.intImageIndex = 0;
                            pictureBox1.Image = Image.FromFile(_listView.Items[_objImageData.intImageIndex].Text);
                            _objImageData.intImageIndex++;

                        }
                        GoFullscreen(_bolFlag);
                    }                   
                }

                else if (e.KeyData == Keys.Left)
                {
                    if (pictureBox1.Image != null)
                    {
                        if (_objImageData.intImageIndex > 0 || _objImageData.intImageIndex <= StaticImageList.Instance.GlobalImageList.Images.Count)
                        {
                            pictureBox1.Image = Image.FromFile(_listView.Items[_objImageData.intImageIndex].Text);
                            _objImageData.intImageIndex--;
                        }
                        else
                        {
                            _objImageData.intImageIndex = 0;
                            pictureBox1.Image = Image.FromFile(_listView.Items[_objImageData.intImageIndex].Text);
                            _objImageData.intImageIndex--;

                        }
                        GoFullscreen(_bolFlag);
                    }
                }
                else
                {
                    MessageBox.Show(_objImageData.strMessage);
                }
            }

            catch (Exception ex)
            {
                Debug.WriteLine("ERROR: " + ex.Message);
            }

            finally
            {
                if (_objImageData.intImageIndex >= StaticImageList.Instance.GlobalImageList.Images.Count)
                {
                    _objImageData.intImageIndex--;
                }
            
                else if (_objImageData.intImageIndex <= -1)
                {
                    _objImageData.intImageIndex++;
                }
                
            }

        }
        #endregion Events

        #region Methods
        /// <summary>
        /// Hides everything and displays black background.
        /// Low coupled and highly cohesive.
        /// </summary>
        /// <param name="fullscreen"></param>
        private void GoFullscreen(bool fullscreen)
        {
            if (fullscreen)
            {
                WindowState = FormWindowState.Normal;
                FormBorderStyle = FormBorderStyle.None;
                Bounds = Screen.PrimaryScreen.Bounds;
                BackColor = Color.Black;
            }
            else
            {
                WindowState = FormWindowState.Maximized;
                FormBorderStyle = FormBorderStyle.Sizable;
            }
        }
        #endregion Methods
    }
}
