﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Data
{
    public class ImageData
    {
        public string strFileName { get; set; }
        public int intCounter { set; get; }
        public int intImageIndex { get; set; }
        public string strMessage { get; set; }

        public ImageData()
        {
            strMessage = "No image in picture box";
            intCounter = 0;
        }
    }
}
